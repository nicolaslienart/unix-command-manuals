# AWK - pattern scanning in file

## Display specific columns of the file

```
# Display columns 2 and 3 separated with a tab from file.txt
awk '{print $2 "\t" $3}' file.txt
```

## Specific line

```
# Display line number 3
awk 'NR == 3' file.txt
```
