# Bluetooth

## Install

Debian: `bluetooth bluez rfkill`

# If the device is blocked

**rfkill** is a tool for enabling and disabling wireless devices

```bash
sudo rfkill list
# If blocked 
sudo rfkill unblock bluetooth
```

## Connect a device
```bash
bluetoothctl power on
bluetoothctl pairable on
bluetoothctl scan on
bluetoothctl pair <device MAC>
bluetoothctl connect <device MAC>
bluetoothctl trust <device MAC>
```
