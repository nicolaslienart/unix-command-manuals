# df - disk space usage
`df [OPTION]... [FILE]...`

## --human-readable, -h
size in human readable format

## --print-type, -T
Show the type of the partition (e.g. ext4, ntfs)
