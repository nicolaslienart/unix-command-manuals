# Disk drive management

## Lisk devices

`lsblk`

## Turn off HDD

```
udisksctl power-off --block-device /dev/sdc
```
