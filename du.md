# du - disk usage utility

`du [OPTION]... [FILE]...`

## --all, -a
For all files, not just directories

## --human-readable, -h 
print sizes in human readable format (e.g., 1K 234M 2G)

## --max-depth=, -d 
Maximum depth, not the entire tree

## --summarize
Max depth=0
display the total for each argument

Useful command with sort
```du --all --human-readable --max-depth=1 . | sort --human-numeric-sort --reverse```

