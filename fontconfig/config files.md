# Config files

```
 00 through 09		Font directories
 10 through 19		system rendering defaults (AA, etc)
 20 through 29		font rendering options
 30 through 39		family substitution
 40 through 49		generic identification, map family->generic
 50 through 59		alternate config file loading
 60 through 69		generic aliases, map generic->family
 70 through 79		select font (adjust which fonts are available)
 80 through 89		match target="scan" (modify scanned patterns)
 90 through 99		font synthesis
```

## Prepend a family, to display emojis fo instance

We want to show color emojis in Firefox which are monochrome by default

```xml
  <match>
    <test name="prgname">
      <string>firefox</string>
    </test>
    <edit name="family" mode="prepend" binding="weak">
      <string>Noto Color Emoji</string>
    </edit>
  </match>
```

[Source: Arch Wiki](https://wiki.archlinux.org/index.php/Font_Configuration)

## Font substitution for a specific program

Here we substitute 'Courier New' to 'monospace' which is a generic family in Firefox
```
  <match>
    <test name="prgname">
      <string>firefox</string>
    </test>
    <test name="family">
      <string>Courier New</string>
    </test>
    <edit name="family" mode="assign">
      <string>monospace</string>
    </edit>
  </match>

```
