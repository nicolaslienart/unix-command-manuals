# Testing

Open this in Firefox
The font underlined in the inspector is the font used
We can also inspect the font panel

```
data:text/html,<meta charset="utf8"><p style="font-family: monospace;">☺</p>
```


Last ones are the ones
```
DISPLAY=:0 FC_DEBUG=4 pango-view --font=monospace -t ☺ | grep family:
```

[Source: Eevee](https://eev.ee/blog/2015/05/20/i-stared-into-the-fontconfig-and-the-fontconfig-stared-back-at-me/)
