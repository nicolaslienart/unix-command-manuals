# GIT

## Create repo locally then push to remote
```bash
git push --set-upstream git@framagit.org:nicolaslienart/my-new-repo-name.git master
```
Or with automatic name detection from local git directory, and branch name
```bash
git push --set-upstream git@framagit.org:nicolaslienart/$(git rev-parse --show-toplevel | xargs basename).git $(git rev-parse --abbrev-ref HEAD)
```

## Set a new origin
```bash
git remote add origin git@framagit.org:unix-command-manuals.git
```

## Unstaging
### Untracked file
`git rm -r --cached [file]`
### Others
`git reset [file]`

## Observe repo
### Check diff on a staged file
```bash
git diff --cached [file]
```
