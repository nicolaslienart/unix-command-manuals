# lsblk - list block (devices)

## Change output
```
-o, --output <list>
```

```
# Show complete path (prepending /dev/)
-p, --paths
```

```
NAME device name (tree, more convenient to distinguish physical disks)
KNAME internal kernel device name (no tree)
TYPE device type (redundant with NAME)
LABEL partition name
UUID filesystem UUID (useful for fstab)
SIZE human-readable device size
FSUSE% filesystem use percentage (only when mounted)
FSTYPE filesystem type (ext4, ntfs, vfat...)
```
