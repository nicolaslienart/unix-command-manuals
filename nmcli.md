# NetworkManager

## Power adapter on/off

```
nmcli radio wifi on
nmcli radio wifi off
```

## List available wifi

```
nmcli device wifi list
```

## Connect to a wifi

```
nmcli device wifi connect "$[B]SSID" password "$PASSWORD"
#Interactive password mode
nmcli --ask device wifi connect "$[B]SSID"

```

`-a | --ask` 
When using this option nmcli will stop and ask for any missing required arguments.

## Properties

```
nmcli --pretty --fields general,wifi-properties device show wlo1
# Short version
nmcli -p -f general,wifi-properties device show wlo1
```
 
