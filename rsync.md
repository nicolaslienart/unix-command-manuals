# Rsync

## Copy with progress
[Progress view](https://askubuntu.com/questions/17275/how-to-show-the-transfer-progress-and-speed-when-copying-files-with-cp)

`-P same as --partial --progress`

`--partial` to keep partially transferred files (do not delete if aborted)

```bash
rsync -ah --progress source-file destination-file
```

## To show progress without listing files

`--info=progress2`

## -z, --compress

Compression will be used to reduce the size of data portions of the transfer.

## Copy recursively to a certain depth
[recursive copy limited in depth](https://unix.stackexchange.com/questions/178362/rsync-recursively-with-a-certain-depth-of-subfolders)

```bash
rsync -r --exclude="/*/" source/ source/yolo/
```

## Include only certain files
```bash
rsync -a --include="*.txt" --include="*.mp3" --exclude="*" from/ to/
```

## Update dest files if source file is more recent
```
-u or --update
```

## Perform a "move" instead of a "copy"
```
--remove-source-files
```
