# Tar

## -C

Execute the command from another directly, useful to choose the output location

```sh
tar -x[z|j]f my/file.tar.[gz|bz] -C other/dir
```

## --strip-components=NUMBER

skip leading components from file name (ex: 1 = skip firstdir of filename)
