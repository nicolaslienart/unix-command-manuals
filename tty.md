# helpful TTY commands

## Controlling brightness

```
echo 105726 > /sys/class/backlight/intel_backlight/brightness
```

Show maximum brightness

```
cat /sys/class/backlight/intel_backlight/max_brightness
```

## Checking battery state

```
upower --show-info /org/freedesktop/UPower/devices/battery_BAT0
```
Get battery hardware path
```
upower --enumerate
```

