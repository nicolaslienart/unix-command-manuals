# Unzip

## output directory
`unzip -x file.zip -d /output/path`

## extract for a pattern
`unzip -x \*.txt`

## extract specified pattern(s) / file(s)
`unzip -x file1 file2 "*.txt" file3.mp3`
