# youtube-dl

## mp3 usage
```bash
youtube-dl --extract-audio --audio-format mp3 <video URL>
```

## -x, --extract-audio
convert video files to audio-only files (requires ffmpeg or avconv and ffprobe or avprobe)

## --embed-thumbnail
As name suggests

## --add-metadata

## -o, --output OUTPUT TEMPLATE"%(title)s.%(ext)s"

Edit output, refer to man page for options

`--output "%(title)s.%(ext)s"` leads to `<video title>.<extenstion>`
