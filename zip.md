# ZIP
## zip recursively and only include certain files

`-r` is to specify a directory

```sh
zip archive_name -r location --include \*.java --include=\*.md
zip archive_name -r location -i \*.java \*.md
zip archive_name -R location \*.java \*.md
```
